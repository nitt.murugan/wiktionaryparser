__author__ = 'murugan'

from BeautifulSoup import BeautifulSoup
from BeautifulSoup import NavigableString, Tag
from urllib2 import urlopen

import re
import sys

if len(sys.argv) < 2:
    print 'Usage : python parse.py searchstring'
    sys.exit(1)

soup = BeautifulSoup(urlopen('https://en.wiktionary.org/wiki/' + sys.argv[1]))

"""
Cleaning up unwanted data
"""
for s in soup('head'):
    s.extract()

for s in soup('script'):
    s.extract()

for s in soup('link'):
    s.extract()

tagNoun = soup.find('a', {'title':'Edit section: Noun'})
tagDefinitions = tagNoun.parent.parent.parent.ol

for line in tagDefinitions:
    result = []
    for descendant in line:
        if isinstance(descendant, NavigableString):
            result.append(unicode(descendant).strip())
        elif isinstance(descendant, Tag):
            if len(result) == 0:
                continue

            if re.match(r'(ul|dl)',descendant.name) is not None:
                break

            if descendant.name == u'a':
                tagLinks = [descendant]
            else:
                tagLinks = descendant.findAll('a')

            for x in tagLinks:
                result.append(unicode(x.text).strip())

    print u' '.join(result)

